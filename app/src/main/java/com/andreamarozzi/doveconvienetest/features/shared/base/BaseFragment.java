package com.andreamarozzi.doveconvienetest.features.shared.base;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

/**
 * Created by amarozzi on 31/05/17.
 */

public class BaseFragment extends Fragment {

    private AsyncTask task;
    private Call call;

    public boolean isValidFragment() {
        return isAdded() && getActivity() != null;
    }

    public void setCall(Call call) {
        this.call = call;
    }

    public void setTask(AsyncTask task) {
        this.task = task;
    }

    @Override
    public void onStop() {
        cancelTask();
        cancelCall();
        super.onStop();
    }

    protected void cancelTask() {
        if (task != null && task.getStatus() != AsyncTask.Status.FINISHED)
            task.cancel(true);
    }

    public void cancelCall() {
        if (call != null)
            call.cancel();
    }

    /**
     * Controlla se la lista dei permessin in ingresso è stata concessa, in caso contrario restituisce
     * la lista dei permessi da chiedere
     *
     * @param permission permissi da controllare
     * @return la lista dei permessi da chiedere
     */
    @NonNull
    public final String[] checkPermission(String... permission) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            List<String> pNotGranted = new ArrayList<>();
            for (int i = 0; i < permission.length; i++) {
                if (!isPermissionGranted(permission[i]))
                    pNotGranted.add(permission[i]);
            }
            return pNotGranted.toArray(new String[pNotGranted.size()]);
        }

        return new String[0];
    }

    /**
     * Controlla se il permesso in ingresso è stato concesso
     *
     * @param permission permesso da controllare.
     *                   Esempio Manifest.permission.READ_SMS, Manifest.permission.SEND_SMS, Manifest.permission.RECEIVE_SMS
     * @return true permesso concesso, false in caso contrario
     */
    @SuppressLint("NewApi")
    public final boolean isPermissionGranted(String permission) {
        return ContextCompat.checkSelfPermission(getActivity(), permission) == PackageManager.PERMISSION_GRANTED;
    }

    protected int getColorApp(int resColor) {
        return ContextCompat.getColor(getActivity(), resColor);
    }
}