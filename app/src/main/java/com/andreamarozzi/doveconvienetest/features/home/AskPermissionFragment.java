package com.andreamarozzi.doveconvienetest.features.home;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.andreamarozzi.doveconvienetest.R;
import com.andreamarozzi.doveconvienetest.app.AppController;
import com.andreamarozzi.doveconvienetest.features.shared.base.BaseFragment;

/**
 * Created by amarozzi on 31/05/17.
 */

public class AskPermissionFragment extends BaseFragment {

    public interface OnAskPermissionListener {

        void onPermissionGranted();
    }

    public static AskPermissionFragment getInstance() {
        return new AskPermissionFragment();
    }

    private static final int REQUEST_PERMISSION = 101;
    private static final int ACTIVITY_SETTINGS = 102;

    private OnAskPermissionListener listener;
    private boolean firstAttemp = true;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnAskPermissionListener)
            setListener((OnAskPermissionListener) context);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof OnAskPermissionListener)
            setListener((OnAskPermissionListener) activity);
    }

    public void setListener(OnAskPermissionListener listener) {
        this.listener = listener;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_ask_fragment, container, false);

        view.findViewById(R.id.request_perm_bt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestPermissions();
            }
        });

        return view;
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void requestPermissions() {
        String[] permResult = checkPermission(AppController.PERMS_TO_ASK);
        if (permResult.length != 0 && firstAttemp) {
            requestPermissions(permResult, REQUEST_PERMISSION);
        } else {
            Intent myAppSettings = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" + getActivity().getPackageName()));
            myAppSettings.addCategory(Intent.CATEGORY_DEFAULT);
            myAppSettings.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivityForResult(myAppSettings, ACTIVITY_SETTINGS);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_PERMISSION) {
            firstAttemp = false;
            String[] permResult = checkPermission(AppController.PERMS_TO_ASK);
            if (permResult.length == 0) {
                if (listener != null)
                    listener.onPermissionGranted();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}