package com.andreamarozzi.doveconvienetest.features.home;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.andreamarozzi.core.model.contidion.Observation;
import com.andreamarozzi.core.model.forecast.Forecast;
import com.andreamarozzi.core.utils.Data;
import com.andreamarozzi.doveconvienetest.R;
import com.andreamarozzi.doveconvienetest.app.AppController;
import com.andreamarozzi.doveconvienetest.features.shared.base.BaseAppActivity;

import java.util.Date;

/**
 * Created by amarozzi on 31/05/17.
 */

public class HomeActivity extends BaseAppActivity implements AskPermissionFragment.OnAskPermissionListener, RetriveDataFragment.OnRetriveDataListener {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        Log.e("Home", Data.getDate(new Date(), "h:mm a zzz 'on' MMMM dd, yyyy"));

        getSupportActionBar().setTitle("");

        String[] permResult = checkPermission(AppController.PERMS_TO_ASK);
        if (permResult.length == 0)
            showRetriveDataFragment();
        else
            setFragment(AskPermissionFragment.getInstance());
    }

    private void showRetriveDataFragment() {
        setFragment(RetriveDataFragment.getInstance());
    }

    @Override
    public void onPermissionGranted() {
        showRetriveDataFragment();
    }

    @Override
    public void onErrorRetrivingCurrentLocation() {
        setFragment(ErrorFragment.getInstance());
    }

    @Override
    public void onErrorRetrivingData() {
        setFragment(ErrorFragment.getInstance());
    }

    @Override
    public void onRetriveCurrentLocation(@NonNull String location) {
        getSupportActionBar().setTitle(location);
    }

    @Override
    public void onRetriveDataCompleted(Observation observation, Forecast forecast) {
        setFragment(WeatherFragment.getInstance(observation, forecast));
    }
}