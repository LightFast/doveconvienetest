package com.andreamarozzi.doveconvienetest.features.home;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.andreamarozzi.core.model.contidion.Observation;
import com.andreamarozzi.core.model.forecast.Forecast;
import com.andreamarozzi.doveconvienetest.R;
import com.andreamarozzi.doveconvienetest.features.shared.adapter.WeatherAdapter;
import com.andreamarozzi.doveconvienetest.features.shared.base.BaseFragment;
import com.andreamarozzi.doveconvienetest.features.shared.decorator.StickyHeaderDecoration;
import com.andreamarozzi.doveconvienetest.features.shared.decorator.VerticalSpaceItemDecoration;

/**
 * Created by amarozzi on 31/05/17.
 */

public class WeatherFragment extends BaseFragment {

    private static final String FORECAST = "forecast";
    private static final String OBSERVATION = "observation";

    public static WeatherFragment getInstance(Observation observation, Forecast forecast) {
        WeatherFragment weatherFragment = new WeatherFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(OBSERVATION, observation);
        bundle.putSerializable(FORECAST, forecast);
        weatherFragment.setArguments(bundle);
        return weatherFragment;
    }

    private RecyclerView recyclerView;
    private StickyHeaderDecoration decoration;
    private WeatherAdapter adapter;

    private Observation observation;
    private Forecast forecast;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_weather, container, false);

        adapter = new WeatherAdapter(getActivity());
        decoration = new StickyHeaderDecoration(adapter);

        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new VerticalSpaceItemDecoration(getResources().getDimensionPixelOffset(R.dimen.divider_height)));
        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration(decoration, 1);

        Bundle bundle = getArguments();
        if (bundle != null) {
            setData((Observation) bundle.getSerializable(OBSERVATION),
                    (Forecast) bundle.getSerializable(FORECAST));
        }

        showUI();

        return view;
    }

    public void setData(Observation observation, Forecast forecast) {
        this.observation = observation;
        this.forecast = forecast;
    }

    public void showUI() {
        if (!isValidFragment())
            return;

        adapter.setObservation(observation);
        adapter.setForecast(forecast);
    }
}
