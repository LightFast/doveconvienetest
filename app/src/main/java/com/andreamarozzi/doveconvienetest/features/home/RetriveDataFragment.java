package com.andreamarozzi.doveconvienetest.features.home;

import android.app.Activity;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.andreamarozzi.core.model.contidion.Condition;
import com.andreamarozzi.core.model.contidion.Observation;
import com.andreamarozzi.core.model.forecast.Forecast;
import com.andreamarozzi.core.model.forecast.Forecast10Day;
import com.andreamarozzi.core.network.RestApi;
import com.andreamarozzi.doveconvienetest.R;
import com.andreamarozzi.doveconvienetest.features.shared.base.BaseFragment;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by amarozzi on 31/05/17.
 */

public class RetriveDataFragment extends BaseFragment implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private static final String TAG = RetriveDataFragment.class.getSimpleName();
    private static final long UPDATE_INTERVAL = 60 * 1000;  /* 60 secs */
    private static final long FASTEST_INTERVAL = 10 * 1000; /* 10 sec */

    public interface OnRetriveDataListener {

        void onErrorRetrivingCurrentLocation();

        void onErrorRetrivingData();

        /**
         * When the app find the location of the device
         *
         * @param location city
         */
        void onRetriveCurrentLocation(@NonNull String location);

        /**
         * The data of the weather
         *
         * @param observation   current weather
         * @param forecast10Day next 10 days
         */
        void onRetriveDataCompleted(Observation observation, Forecast forecast10Day);
    }

    public static RetriveDataFragment getInstance() {
        return new RetriveDataFragment();
    }

    private OnRetriveDataListener listener;
    private GoogleApiClient googleApi;
    private String country;
    private String locality;
    private Observation observation;
    private Forecast forecast;

    private Callback<Condition> observationCallback = new Callback<Condition>() {
        @Override
        public void onResponse(Call<Condition> call, Response<Condition> response) {
            if (response.isSuccessful() && response.body() != null) {
                observation = response.body().getCurrent_observation();
                retriveForecast10Day();
            } else {
                onFailure(call, null);
            }
        }

        @Override
        public void onFailure(Call<Condition> call, Throwable t) {
            Log.e(TAG, "Errore durante il get observation", t);
            if (listener != null)
                listener.onErrorRetrivingData();
        }
    };

    private Callback<Forecast10Day> forecastCallback = new Callback<Forecast10Day>() {
        @Override
        public void onResponse(Call<Forecast10Day> call, Response<Forecast10Day> response) {
            if (response.isSuccessful() && response.body() != null) {
                forecast = response.body().getForecast();
                checkAndShowData();
            } else {
                onFailure(call, null);
            }
        }

        @Override
        public void onFailure(Call<Forecast10Day> call, Throwable t) {
            Log.e(TAG, "Errore durante il get forecast", t);
            if (listener != null)
                listener.onErrorRetrivingData();
        }
    };

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnRetriveDataListener)
            setListener((OnRetriveDataListener) context);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof OnRetriveDataListener)
            setListener((OnRetriveDataListener) activity);
    }

    public RetriveDataFragment setListener(OnRetriveDataListener listener) {
        this.listener = listener;
        return this;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_retrive_data, container, false);

        googleApi = new GoogleApiClient.Builder(getActivity())
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();

        return view;
    }

    public void onResume() {
        super.onResume();
        googleApi.connect();
    }

    public void onPause() {
        stopLocationUpdates();
        super.onPause();
    }

    @Override
    public void onConnected(Bundle dataBundle) {
        Location location = LocationServices.FusedLocationApi.getLastLocation(googleApi);
        if (location != null) {
            onLocationFound(location.getLatitude(), location.getLongitude());
        } else {
            startLocationUpdates();
        }
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if (listener != null)
            listener.onErrorRetrivingCurrentLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {
        if (listener != null)
            listener.onErrorRetrivingCurrentLocation();
    }

    public void onLocationChanged(Location location) {
        onLocationFound(location.getLatitude(), location.getLongitude());
    }

    private void startLocationUpdates() {
        LocationRequest locRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY)
                .setInterval(UPDATE_INTERVAL)
                .setFastestInterval(FASTEST_INTERVAL);

        LocationServices.FusedLocationApi.requestLocationUpdates(googleApi, locRequest, this);
    }

    private void stopLocationUpdates() {
        if (googleApi != null && googleApi.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(googleApi, this);
            googleApi.disconnect();
        }
    }

    private void onLocationFound(double lat, double lon) {
        stopLocationUpdates();
        setTask(new AddressFromLocation(getActivity()).execute(lat, lon));
    }

    private void onAddressFound(@Nullable Address address) {
        if (address == null) {
            if (listener != null)
                listener.onErrorRetrivingCurrentLocation();
            return;
        }

        country = address.getCountryCode();
        locality = address.getLocality();

        if (listener != null)
            listener.onRetriveCurrentLocation(locality);

        retriveCurrentObservation();
    }

    private void retriveCurrentObservation() {
        setCall(RestApi.getConditions(country, locality, observationCallback));
    }

    private void retriveForecast10Day() {
        setCall(RestApi.getForecast10Day(country, locality, forecastCallback));
    }

    private void checkAndShowData() {
        if (observation != null && forecast != null) {
            if (listener != null)
                listener.onRetriveDataCompleted(observation, forecast);
        } else {
            if (listener != null)
                listener.onErrorRetrivingData();
        }
    }

    private class AddressFromLocation extends AsyncTask<Double, Void, Address> {

        private Context context;

        private AddressFromLocation(Context context) {
            this.context = context;
        }

        @Override
        protected Address doInBackground(Double... params) {
            try {
                List<Address> addresses = new Geocoder(context).getFromLocation(params[0], params[1], 1);
                if (addresses != null && addresses.size() > 0) {
                    return addresses.get(0);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Address address) {
            onAddressFound(address);
        }
    }
}