package com.andreamarozzi.doveconvienetest.features.shared.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.andreamarozzi.core.model.contidion.Observation;
import com.andreamarozzi.core.model.forecast.Forecast;
import com.andreamarozzi.core.model.forecast.simple.SimpleForecastDay;
import com.andreamarozzi.core.model.forecast.text.TextForecastday;
import com.andreamarozzi.core.utils.Data;
import com.andreamarozzi.doveconvienetest.R;
import com.andreamarozzi.doveconvienetest.features.shared.decorator.StickyHeaderDecoration;
import com.squareup.picasso.Picasso;

/**
 * Created by amarozzi on 05/06/17.
 */

public class WeatherAdapter extends RecyclerView.Adapter<WeatherAdapter.ViewHolder> implements
        StickyHeaderAdapter<WeatherAdapter.HeaderHolder> {

    private LayoutInflater mInflater;
    private Forecast forecast;
    private Observation observation;

    public WeatherAdapter(Context context) {
        mInflater = LayoutInflater.from(context);
    }

    public void setObservation(Observation observation) {
        this.observation = observation;
    }

    public void setForecast(Forecast forecast) {
        this.forecast = forecast;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new ViewHolder(mInflater.inflate(R.layout.item_forecastday, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        SimpleForecastDay simple = forecast.getSimpleForecastDay(position);
        TextForecastday text = forecast.getTextForecast(simple.getPeriod() - 1);

        String date = Data.getDate(simple.getDate().getDate(), "EEEE dd MMMM");
        date = date.substring(0, 1).toUpperCase() + date.substring(1);
        holder.day.setText(date);
        holder.forecast.setText(text.getFcttext_metric());
        holder.temperature.setText(holder.temperature.getContext().getString(R.string.temperature_max_min,
                (int) simple.getHigh().getCelsius(), (int) simple.getLow().getCelsius()));

        Picasso.with(holder.icon.getContext())
                .load(simple.getIcon_url())
                .resizeDimen(R.dimen.forecast_icon_size, R.dimen.forecast_icon_size)
                .into(holder.icon);
    }

    @Override
    public int getItemCount() {
        return forecast != null ? 10 : 0;
    }

    @Override
    public long getHeaderId(int position) {
        if (observation == null) {
            return StickyHeaderDecoration.NO_HEADER_ID;
        }
        return (long) 0;
    }

    @Override
    public HeaderHolder onCreateHeaderViewHolder(ViewGroup parent) {
        final View view = mInflater.inflate(R.layout.item_observation, parent, false);
        return new HeaderHolder(view);
    }

    @Override
    public void onBindHeaderViewHolder(HeaderHolder viewholder, int position) {
        viewholder.temperature.setText(viewholder.temperature.getContext().getString(R.string.temperature, observation.getTemp_c()));
        viewholder.weather.setText(observation.getWeather());
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView day;
        private TextView forecast;
        private TextView temperature;
        private ImageView icon;

        public ViewHolder(View view) {
            super(view);
            day = (TextView) view.findViewById(R.id.forecast_day);
            forecast = (TextView) view.findViewById(R.id.forecast_text);
            temperature = (TextView) view.findViewById(R.id.forecast_temperature);
            icon = (ImageView) view.findViewById(R.id.forecast_icon);
        }
    }

    static class HeaderHolder extends RecyclerView.ViewHolder {
        public TextView temperature;
        public TextView weather;

        public HeaderHolder(View view) {
            super(view);
            temperature = (TextView) view.findViewById(R.id.observation_temperature);
            weather = (TextView) view.findViewById(R.id.observation_weather);
        }
    }
}