package com.andreamarozzi.doveconvienetest.features.shared.base;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.andreamarozzi.doveconvienetest.R;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

/**
 * Created by amarozzi on 31/05/17.
 */

public abstract class BaseAppActivity extends AppCompatActivity {

    private Call call;
    private AsyncTask task;

    public void setTask(AsyncTask task) {
        this.task = task;
    }

    public void setCall(Call call) {
        this.call = call;
    }

    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(this);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    protected void onStop() {
        cancelTask();
        cancelCall();
        super.onStop();
    }

    protected void cancelCall() {
        if (call != null)
            call.cancel();
    }

    protected void cancelTask() {
        if (task != null && task.getStatus() != AsyncTask.Status.FINISHED)
            task.cancel(true);
    }

    protected int getColorApp(int resColor) {
        return ContextCompat.getColor(this, resColor);
    }

    /**
     * Controlla se la lista dei permessin in ingresso è stata concessa, in caso contrario restituisce
     * la lista dei permessi da chiedere
     *
     * @param permission permissi da controllare
     * @return la lista dei permessi da chiedere
     */
    @NonNull
    public final String[] checkPermission(String... permission) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            List<String> pNotGranted = new ArrayList<>();
            for (int i = 0; i < permission.length; i++) {
                if (!isPermissionGranted(permission[i]))
                    pNotGranted.add(permission[i]);
            }
            return pNotGranted.toArray(new String[pNotGranted.size()]);
        }

        return new String[0];
    }

    /**
     * Controlla se il permesso in ingresso è stato concesso
     *
     * @param permission permesso da controllare.
     *                   Esempio Manifest.permission.READ_SMS, Manifest.permission.SEND_SMS, Manifest.permission.RECEIVE_SMS
     * @return true permesso concesso, false in caso contrario
     */
    @SuppressLint("NewApi")
    public final boolean isPermissionGranted(String permission) {
        return ContextCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED;
    }

    /**
     * Convenience for calling {@link #setFragment(Fragment, int, boolean)}.
     * con int <code>R.id.container</code> e boolean <code>false</code>
     *
     * @param frag
     */
    protected void setFragment(Fragment frag) {
        setFragment(frag, R.id.container, false);
    }

    protected void addFragment(Fragment frag) {
        addFragment(frag, R.id.container, false);
    }

    protected void setFragment(Fragment frag, boolean addBack) {
        setFragment(frag, R.id.container, addBack);
    }

    protected void addFragment(Fragment frag, boolean addBack) {
        addFragment(frag, R.id.container, addBack);
    }

    /**
     * Convenience for calling {@link #setFragment(Fragment, int, String, boolean)}.
     * con String data <code>Fragment.getClass().getNome();</code>
     *
     * @param frag
     * @param idContainer
     * @param addBack
     */
    protected void setFragment(Fragment frag, int idContainer, boolean addBack) {
        setFragment(frag, idContainer, frag.getClass().getName(), addBack);
    }

    protected void addFragment(Fragment frag, int idContainer, boolean addBack) {
        addFragment(frag, idContainer, frag.getClass().getName(), addBack);
    }

    /**
     * Se the fragment on the view indicate by the container
     *
     * @param frag      fragment to set
     * @param container view to put the fragment
     * @param tag       tag for identifier the fragment on the activity
     */
    protected void setFragment(Fragment frag, int container, String tag, boolean addBack) {
        if (isFinishing())
            return;

        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(container, frag, tag);
        if (addBack)
            transaction.addToBackStack(tag);
        transaction.commitAllowingStateLoss();
    }

    protected void addFragment(Fragment frag, int container, String tag, boolean addBack) {
        if (isFinishing())
            return;

        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.add(container, frag, tag);
        if (addBack)
            transaction.addToBackStack(tag);
        transaction.commitAllowingStateLoss();
    }

    protected boolean removeFragment(int id) {
        return removeFragment((Fragment) getFragment(id));
    }

    protected boolean removeFragment(Class c) {
        return removeFragment((Fragment) getFragment(c));
    }

    protected boolean removeFragment(String tag) {
        return removeFragment((Fragment) getFragment(tag));
    }

    protected boolean removeFragment(Fragment frag) {
        if (frag == null)
            return false;
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.remove(frag);
        transaction.commitAllowingStateLoss();

        return true;
    }

    protected boolean isFragmentOnScreen(Class tag) {
        return isFragmentOnScreen(tag.getName());
    }

    protected boolean isFragmentOnScreen(String tag) {
        Fragment frag = getFragmentManager().findFragmentByTag(tag);
        return frag != null;
    }

    @SuppressWarnings("unchecked")
    protected <T extends Fragment> T getFragment(int id) {
        Fragment frag = getFragmentManager().findFragmentById(id);
        if (frag != null)
            return (T) frag;

        return null;
    }

    protected <T extends Fragment> T getFragment(Class c) {
        return getFragment(c.getName());
    }

    @SuppressWarnings("unchecked")
    protected <T extends Fragment> T getFragment(String tag) {
        Fragment frag = getFragmentManager().findFragmentByTag(tag);
        if (frag != null)
            return (T) frag;

        return null;
    }

    @Override
    public void onBackPressed() {
        if (getNumItemBackStack() > 0) {
            getFragmentManager().popBackStack();
            onPopBackStack();
        } else {
            super.onBackPressed();
        }
    }

    /**
     * Call when the activity resume a fragment from the stack
     */
    protected void onPopBackStack() {

    }

    public int getNumItemBackStack() {
        return getFragmentManager().getBackStackEntryCount();
    }

    public void clearStackFragment() {
        getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }
}