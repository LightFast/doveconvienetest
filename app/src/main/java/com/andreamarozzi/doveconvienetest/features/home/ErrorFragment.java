package com.andreamarozzi.doveconvienetest.features.home;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.andreamarozzi.doveconvienetest.R;
import com.andreamarozzi.doveconvienetest.features.shared.base.BaseFragment;

/**
 * Created by amarozzi on 31/05/17.
 */

public class ErrorFragment extends BaseFragment {

    public static Fragment getInstance() {
        return new ErrorFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_error, container, false);
        return view;
    }
}