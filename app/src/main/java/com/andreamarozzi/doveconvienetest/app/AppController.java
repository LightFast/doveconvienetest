package com.andreamarozzi.doveconvienetest.app;

import android.Manifest;
import android.app.Application;

/**
 * Created by amarozzi on 31/05/17.
 */

public class AppController extends Application {

    /**
     * Permission to ask for use this application
     */
    public static final String[] PERMS_TO_ASK = new String[]{
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
    };

    @Override
    public void onCreate() {
        super.onCreate();
    }
}
