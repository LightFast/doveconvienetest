# Dove Conviene Test

Test per doveconviene.it, l'app consente la visualizzazione delle condizioni atmosferiche attuali e 
quelle previste nei prossimi 10 giorni.

![](device-2017-06-05-142851.png) ![](device-2017-06-05-142905.png) ![](device-2017-06-05-143005.png)

[Video](https://vimeo.com/220303964)

## Utilizzo

L'applicazione all'avvio richiede la concessione del permesso di geolocalizzazione. Una volta ottenuto 
il permesso l'app avvia il recupero dell'ultima posizione nota tramite le APIs di Google o ne aspetta 
la ricezione di nuove. Ricavata la posizione del device vengono recuperati la città e lo stato. 
Tramite le APIs di [Wunderground](https://www.wunderground.com/) si ricavano le condizioni meteo 
attuali e quelle previste per i prossimi 10 giorni.

## Librerie utilizzate

-  [Retrofit](https://github.com/square/retrofit) libreria utilizzata per effettuare le chiamate RESTAPI, 
è un versione avanzata di OkHttp e un wrapper di GSON di Google. 
-  [Picasso](https://github.com/square/picasso) libreria utilizzata per recuperare e visualizzare le 
immagini dinamicamente all'interno delle ImageView

## Meta

[Andrea Marozzi](http://andreamarozzi.com) - andmaroz89@gmail.com

Distributed under the [WTFPL](http://www.wtfpl.net/) license.