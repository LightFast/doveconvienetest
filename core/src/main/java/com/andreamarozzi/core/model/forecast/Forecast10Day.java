package com.andreamarozzi.core.model.forecast;

import com.andreamarozzi.core.model.Response;

/**
 * Created by amarozzi on 31/05/17.
 */

public class Forecast10Day {

    private Response response;
    private Forecast forecast;

    public Response getResponse() {
        return response;
    }

    public Forecast getForecast() {
        return forecast;
    }
}