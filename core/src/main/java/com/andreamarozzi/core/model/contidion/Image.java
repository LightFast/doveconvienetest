package com.andreamarozzi.core.model.contidion;

import java.io.Serializable;

/**
 * Created by amarozzi on 31/05/17.
 */

public class Image implements Serializable {

    private String url;
    private String title;
    private String link;

    public String getUrl() {
        return url;
    }

    public String getTitle() {
        return title;
    }

    public String getLink() {
        return link;
    }
}