package com.andreamarozzi.core.model.contidion;

import java.io.Serializable;

/**
 * Created by amarozzi on 31/05/17.
 */

public class DisplayLocation extends Location implements Serializable {

    private String state_name;
    private String zip;

    public String getState_name() {
        return state_name;
    }

    public String getZip() {
        return zip;
    }
}