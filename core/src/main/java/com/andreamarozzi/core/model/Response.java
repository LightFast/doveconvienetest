package com.andreamarozzi.core.model;

import com.andreamarozzi.core.model.contidion.Feature;

/**
 * Created by amarozzi on 31/05/17.
 */

public class Response {

    private String version;
    private String termsofService;
    private Feature features;

    public String getVersion() {
        return version;
    }

    public String getTermsofService() {
        return termsofService;
    }

    public Feature getFeatures() {
        return features;
    }
}