package com.andreamarozzi.core.model.forecast.simple;

import java.io.Serializable;

/**
 * Created by amarozzi on 31/05/17.
 */

public class Snow implements Serializable {

    private float in;
    private float cm;

    public float getIn() {
        return in;
    }

    public float getCm() {
        return cm;
    }
}