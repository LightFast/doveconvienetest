package com.andreamarozzi.core.model.forecast.simple;

import java.io.Serializable;

/**
 * Created by amarozzi on 31/05/17.
 */

public class SimpleForecastDay implements Serializable {

    private Date date;
    private int period;
    private Temperature high;
    private Temperature low;
    private String conditions;
    private String icon;
    private String icon_url;
    private String skyicon;
    private int pop;
    private Precipitation qpf_allday;
    private Precipitation qpf_day;
    private Precipitation qpf_night;
    private Snow snow_allday;
    private Snow snow_day;
    private Snow snow_night;
    private Wind maxwind;
    private Wind avewind;
    private int avehumidity;
    private int maxhumidity;
    private int minhumidity;

    public Date getDate() {
        return date;
    }

    public int getPeriod() {
        return period;
    }

    public Temperature getHigh() {
        return high;
    }

    public Temperature getLow() {
        return low;
    }

    public String getConditions() {
        return conditions;
    }

    public String getIcon() {
        return icon;
    }

    public String getIcon_url() {
        return icon_url;
    }

    public String getSkyicon() {
        return skyicon;
    }

    public int getPop() {
        return pop;
    }

    public Precipitation getQpf_allday() {
        return qpf_allday;
    }

    public Precipitation getQpf_day() {
        return qpf_day;
    }

    public Precipitation getQpf_night() {
        return qpf_night;
    }

    public Snow getSnow_allday() {
        return snow_allday;
    }

    public Snow getSnow_day() {
        return snow_day;
    }

    public Snow getSnow_night() {
        return snow_night;
    }

    public Wind getMaxwind() {
        return maxwind;
    }

    public Wind getAvewind() {
        return avewind;
    }

    public int getAvehumidity() {
        return avehumidity;
    }

    public int getMaxhumidity() {
        return maxhumidity;
    }

    public int getMinhumidity() {
        return minhumidity;
    }
}