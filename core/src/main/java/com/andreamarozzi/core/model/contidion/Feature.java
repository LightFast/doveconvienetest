package com.andreamarozzi.core.model.contidion;

import java.io.Serializable;

/**
 * Created by amarozzi on 31/05/17.
 */

public class Feature implements Serializable {

    private int conditions;
    private int forecast10day;

    public int getConditions() {
        return conditions;
    }

    public int getForecast10day() {
        return forecast10day;
    }
}