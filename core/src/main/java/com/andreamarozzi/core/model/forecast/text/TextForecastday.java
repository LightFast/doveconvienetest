package com.andreamarozzi.core.model.forecast.text;

import java.io.Serializable;

/**
 * Created by amarozzi on 05/06/17.
 */

public class TextForecastday implements Serializable {

    private int period;
    private String icon;
    private String icon_url;
    private String title;
    private String fcttext;
    private String fcttext_metric;
    private String pop;

    public int getPeriod() {
        return period;
    }

    public String getIcon() {
        return icon;
    }

    public String getIcon_url() {
        return icon_url;
    }

    public String getTitle() {
        return title;
    }

    public String getFcttext() {
        return fcttext;
    }

    public String getFcttext_metric() {
        return fcttext_metric;
    }

    public String getPop() {
        return pop;
    }
}
