package com.andreamarozzi.core.model.contidion;

import com.andreamarozzi.core.model.Response;

/**
 * Created by amarozzi on 31/05/17.
 */

public class Condition {

    private Response response;
    private Observation current_observation;

    public Response getResponse() {
        return response;
    }

    public Observation getCurrent_observation() {
        return current_observation;
    }
}