package com.andreamarozzi.core.model.contidion;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by amarozzi on 31/05/17.
 */

public class Observation  implements Serializable {

    private Image image;
    private DisplayLocation display_location;
    private Location observation_location;
    private String station_id;
    private String observation_time;
    private Date observation_time_rfc822;//"Wed, 27 Jun 2012 17:27:13 -0700",
    private String observation_epoch;
    private Date local_time_rfc822;//"Wed, 27 Jun 2012 17:27:14 -0700",
    private String local_epoch;
    private String local_tz_short;
    private String local_tz_long;
    private String local_tz_offset;
    private String weather;
    private String temperature_string;
    private float temp_f;
    private float temp_c;
    private String relative_humidity;
    private String wind_string;
    private String wind_dir;
    private int wind_degrees;
    private float wind_mph;
    private float wind_gust_mph;
    private float wind_kph;
    private float wind_gust_kph;
    private String pressure_mb;
    private String pressure_in;
    private String pressure_trend;
    private String dewpoint_string;
    private float dewpoint_f;
    private float dewpoint_c;
    private String heat_index_string;
    private String heat_index_f;
    private String heat_index_c;
    private String windchill_string;
    private String windchill_f;
    private String windchill_c;
    private String feelslike_string;
    private String feelslike_f;
    private String feelslike_c;
    private String visibility_mi;
    private String visibility_km;
    private String solarradiation;
    private String UV;
    private String precip_1hr_string;
    private String precip_1hr_in;
    private String precip_1hr_metric;
    private String precip_today_string;
    private String precip_today_in;
    private String precip_today_metric;
    private String icon;
    private String icon_url;
    private String forecast_url;
    private String history_url;
    private String ob_url;

    public Image getImage() {
        return image;
    }

    public DisplayLocation getDisplay_location() {
        return display_location;
    }

    public Location getObservation_location() {
        return observation_location;
    }

    public String getStation_id() {
        return station_id;
    }

    public String getObservation_time() {
        return observation_time;
    }

    public Date getObservation_time_rfc822() {
        return observation_time_rfc822;
    }

    public String getObservation_epoch() {
        return observation_epoch;
    }

    public Date getLocal_time_rfc822() {
        return local_time_rfc822;
    }

    public String getLocal_epoch() {
        return local_epoch;
    }

    public String getLocal_tz_short() {
        return local_tz_short;
    }

    public String getLocal_tz_long() {
        return local_tz_long;
    }

    public String getLocal_tz_offset() {
        return local_tz_offset;
    }

    public String getWeather() {
        return weather;
    }

    public String getTemperature_string() {
        return temperature_string;
    }

    public float getTemp_f() {
        return temp_f;
    }

    public float getTemp_c() {
        return temp_c;
    }

    public String getRelative_humidity() {
        return relative_humidity;
    }

    public String getWind_string() {
        return wind_string;
    }

    public String getWind_dir() {
        return wind_dir;
    }

    public int getWind_degrees() {
        return wind_degrees;
    }

    public float getWind_mph() {
        return wind_mph;
    }

    public float getWind_gust_mph() {
        return wind_gust_mph;
    }

    public float getWind_kph() {
        return wind_kph;
    }

    public float getWind_gust_kph() {
        return wind_gust_kph;
    }

    public String getPressure_mb() {
        return pressure_mb;
    }

    public String getPressure_in() {
        return pressure_in;
    }

    public String getPressure_trend() {
        return pressure_trend;
    }

    public String getDewpoint_string() {
        return dewpoint_string;
    }

    public float getDewpoint_f() {
        return dewpoint_f;
    }

    public float getDewpoint_c() {
        return dewpoint_c;
    }

    public String getHeat_index_string() {
        return heat_index_string;
    }

    public String getHeat_index_f() {
        return heat_index_f;
    }

    public String getHeat_index_c() {
        return heat_index_c;
    }

    public String getWindchill_string() {
        return windchill_string;
    }

    public String getWindchill_f() {
        return windchill_f;
    }

    public String getWindchill_c() {
        return windchill_c;
    }

    public String getFeelslike_string() {
        return feelslike_string;
    }

    public String getFeelslike_f() {
        return feelslike_f;
    }

    public String getFeelslike_c() {
        return feelslike_c;
    }

    public String getVisibility_mi() {
        return visibility_mi;
    }

    public String getVisibility_km() {
        return visibility_km;
    }

    public String getSolarradiation() {
        return solarradiation;
    }

    public String getUV() {
        return UV;
    }

    public String getPrecip_1hr_string() {
        return precip_1hr_string;
    }

    public String getPrecip_1hr_in() {
        return precip_1hr_in;
    }

    public String getPrecip_1hr_metric() {
        return precip_1hr_metric;
    }

    public String getPrecip_today_string() {
        return precip_today_string;
    }

    public String getPrecip_today_in() {
        return precip_today_in;
    }

    public String getPrecip_today_metric() {
        return precip_today_metric;
    }

    public String getIcon() {
        return icon;
    }

    public String getIcon_url() {
        return icon_url;
    }

    public String getForecast_url() {
        return forecast_url;
    }

    public String getHistory_url() {
        return history_url;
    }

    public String getOb_url() {
        return ob_url;
    }
}
