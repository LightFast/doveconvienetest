package com.andreamarozzi.core.model.forecast.simple;

import java.io.Serializable;

/**
 * Created by amarozzi on 31/05/17.
 */

public class Precipitation implements Serializable {

    private float in;
    private float mm;

    public float getIn() {
        return in;
    }

    public float getMm() {
        return mm;
    }
}