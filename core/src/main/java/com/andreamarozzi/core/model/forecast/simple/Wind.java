package com.andreamarozzi.core.model.forecast.simple;

import java.io.Serializable;

/**
 * Created by amarozzi on 31/05/17.
 */

public class Wind implements Serializable {

    private float mph;
    private float kph;
    private String dir;
    private int degrees;

    public float getMph() {
        return mph;
    }

    public float getKph() {
        return kph;
    }

    public String getDir() {
        return dir;
    }

    public int getDegrees() {
        return degrees;
    }
}