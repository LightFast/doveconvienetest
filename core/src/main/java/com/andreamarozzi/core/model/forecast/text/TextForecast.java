package com.andreamarozzi.core.model.forecast.text;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by amarozzi on 05/06/17.
 */

public class TextForecast implements Serializable {

    private Date date;
    private List<TextForecastday> forecastday;

    public Date getDate() {
        return date;
    }

    public List<TextForecastday> getForecastday() {
        return forecastday;
    }

    public TextForecastday getForecastday(int day) {
        return forecastday != null && day < forecastday.size() ? forecastday.get(day) : null;
    }
}