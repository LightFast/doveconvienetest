package com.andreamarozzi.core.model.forecast.simple;

import java.io.Serializable;
import java.util.GregorianCalendar;

/**
 * Created by amarozzi on 31/05/17.
 */

public class Date implements Serializable {

    private String epoch;
    private String pretty;
    private int day;
    private int month;
    private int year;
    private int yday;
    private int hour;
    private int min;
    private int sec;
    private String isdst;
    private String monthname;
    private String weekday_short;
    private String weekday;
    private String ampm;
    private String tz_short;
    private String tz_long;

    public java.util.Date getDate() {
        return new GregorianCalendar(year, month - 1, day, hour, min, sec).getTime();
    }

    public String getEpoch() {
        return epoch;
    }

    public String getPretty() {
        return pretty;
    }

    public int getDay() {
        return day;
    }

    public int getMonth() {
        return month;
    }

    public int getYear() {
        return year;
    }

    public int getYday() {
        return yday;
    }

    public int getHour() {
        return hour;
    }

    public int getMin() {
        return min;
    }

    public int getSec() {
        return sec;
    }

    public String getIsdst() {
        return isdst;
    }

    public String getMonthname() {
        return monthname;
    }

    public String getWeekday_short() {
        return weekday_short;
    }

    public String getWeekday() {
        return weekday;
    }

    public String getAmpm() {
        return ampm;
    }

    public String getTz_short() {
        return tz_short;
    }

    public String getTz_long() {
        return tz_long;
    }
}