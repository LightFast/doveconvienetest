package com.andreamarozzi.core.model.contidion;

import java.io.Serializable;

/**
 * Created by amarozzi on 31/05/17.
 */

public class Location  implements Serializable {

    private String country;
    private String country_iso3166;
    private String state;
    private String full;
    private String city;
    private double latitude;
    private double longitude;
    private String elevation;

    public String getCountry() {
        return country;
    }

    public String getCountry_iso3166() {
        return country_iso3166;
    }

    public String getState() {
        return state;
    }

    public String getFull() {
        return full;
    }

    public String getCity() {
        return city;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public String getElevation() {
        return elevation;
    }
}
