package com.andreamarozzi.core.model.forecast.simple;

import java.io.Serializable;
import java.util.List;

/**
 * Created by amarozzi on 31/05/17.
 */

public class SimpleForecast implements Serializable {

    private List<SimpleForecastDay> forecastday;

    public List<SimpleForecastDay> getForecastDay() {
        return forecastday;
    }

    public SimpleForecastDay getForecastDay(int day) {
        return forecastday != null && day < forecastday.size() ? forecastday.get(day) : null;
    }
}