package com.andreamarozzi.core.model.forecast;

import com.andreamarozzi.core.model.forecast.simple.SimpleForecast;
import com.andreamarozzi.core.model.forecast.simple.SimpleForecastDay;
import com.andreamarozzi.core.model.forecast.text.TextForecast;
import com.andreamarozzi.core.model.forecast.text.TextForecastday;

import java.io.Serializable;

/**
 * Created by amarozzi on 31/05/17.
 */

public class Forecast implements Serializable {

    private TextForecast txt_forecast;
    private SimpleForecast simpleforecast;

    public SimpleForecast getSimpleForecast() {
        return simpleforecast;
    }

    public TextForecast getTextForecast() {
        return txt_forecast;
    }

    public SimpleForecastDay getSimpleForecastDay(int day) {
        return simpleforecast != null ? simpleforecast.getForecastDay(day) : null;
    }

    public TextForecastday getTextForecast(int day) {
        return txt_forecast != null ? txt_forecast.getForecastday(day) : null;
    }
}
