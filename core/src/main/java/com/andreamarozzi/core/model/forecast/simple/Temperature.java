package com.andreamarozzi.core.model.forecast.simple;

import java.io.Serializable;

/**
 * Created by amarozzi on 31/05/17.
 */

public class Temperature implements Serializable {

    private float fahrenheit;
    private float celsius;

    public float getFahrenheit() {
        return fahrenheit;
    }

    public float getCelsius() {
        return celsius;
    }
}