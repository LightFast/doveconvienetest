package com.andreamarozzi.core.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by amarozzi on 31/05/17.
 */

public class Data {

    public static final String DATE_PATTERN_IT = "dd/MM/yyyy";
    public static final String DATE_PATTERN_GG_MM_YYYY = "dd-MM-yyyy";
    public static final String DATETIME_PATTERN_IT = "dd/MM/yyyy HH:mm:ss";
    public static final String DATE_PATTERN_SERVER = "yyyy-MM-dd";
    public static final String DATETIME_PATTERN_SERVER = "yyyy-MM-dd HH:mm:ss";
    public static final String DATETIME_PATTERN_LOCAL = "yyyyMMdd'_'HHmmss";

    public static final long DAY_IN_MILLIS = 1000 * 60 * 60 * 24;

    /**
     * Converte una data in una stringa
     *
     * @param data
     * @param datePattern
     * @return
     */
    public static String getDate(Date data, String datePattern) {
        return getDate(data, datePattern, Locale.getDefault());
    }

    /**
     * Converte una data in una stringa
     *
     * @param data
     * @param datePattern
     * @param locale
     * @return
     */
    public static String getDate(Date data, String datePattern, Locale locale) {
        DateFormat df = new SimpleDateFormat(datePattern, locale);
        return df.format(data);
    }

    /**
     * Converta una stringa in una data
     *
     * @param dateString
     * @param datePattern
     * @return
     * @throws ParseException
     */
    public static Date getDate(String dateString, String datePattern) throws ParseException {
        return getDate(dateString, datePattern, Locale.getDefault());
    }

    /**
     * Converta una stringa in una data
     *
     * @param dateString
     * @param datePattern
     * @param locale
     * @return
     * @throws ParseException
     */
    public static Date getDate(String dateString, String datePattern, Locale locale) throws ParseException {
        DateFormat df = new SimpleDateFormat(datePattern, locale);
        return df.parse(dateString);
    }
}
