package com.andreamarozzi.core.utils;

import android.util.Log;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.Locale;

/**
 * Created by amarozzi on 31/05/17.
 */

public class DateDeserializer implements JsonDeserializer<Date> {

    public static final DateDeserializer INSTANCE = new DateDeserializer();

    private DateDeserializer() {
    }

    @Override
    public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        String dateS = json.getAsString();
        try {
            int lenght = dateS.length();
            switch (lenght) {
                case 26:
                    return Data.getDate(dateS, "EEE, d MMM yyyy HH:mm:ss Z", Locale.ITALY);//"Wed, 27 Jun 2012 17:27:13 -0700"
                case 29:
                    return Data.getDate(dateS, "h:mm a zzz 'on' MMMM dd, yyyy");//"11:00 PM PDT on July 03, 2012"
            }
        } catch (Exception e) {
            Log.e("Retrofit", "Errore parse date, date to parse: " + dateS, e);
            return null;
        }
        Log.i("Retrofit", "Unable to find the Date Parser Format, date " + dateS);
        return null;
    }
}
