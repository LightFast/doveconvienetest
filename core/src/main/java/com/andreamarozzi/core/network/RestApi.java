package com.andreamarozzi.core.network;

import com.andreamarozzi.core.BuildConfig;
import com.andreamarozzi.core.model.contidion.Condition;
import com.andreamarozzi.core.model.forecast.Forecast10Day;
import com.andreamarozzi.core.network.request.WeatherApi;
import com.andreamarozzi.core.utils.DateDeserializer;
import com.google.gson.GsonBuilder;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by amarozzi on 31/05/17.
 */

public class RestApi {

    private static final String BASE_URL = "http://api.wunderground.com/api/";
    private static final String API_KEY = "83ec6eafa9fc2796";


    private static Retrofit retrofit;

    static {
        final GsonBuilder builder = new GsonBuilder()
                .registerTypeAdapter(Date.class, DateDeserializer.INSTANCE);

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor()
                .setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .build();

        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(builder.create()))
                .build();
    }

    public static Call getConditions(String country, String locality, Callback<Condition> callback) {
        Call<Condition> call = retrofit.create(WeatherApi.class).getConditions(API_KEY, country, locality);
        call.enqueue(callback);
        return call;
    }

    public static Call getForecast10Day(String country, String locality, Callback<Forecast10Day> callback) {
        Call<Forecast10Day> call = retrofit.create(WeatherApi.class).getForecast10Day(API_KEY, country, locality);
        call.enqueue(callback);
        return call;
    }

}
