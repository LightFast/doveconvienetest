package com.andreamarozzi.core.network.request;

import com.andreamarozzi.core.model.contidion.Condition;
import com.andreamarozzi.core.model.forecast.Forecast10Day;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by amarozzi on 31/05/17.
 */

public interface WeatherApi {

    @GET("{ApiKey}/conditions/q/{Country}/{Locality}.json")
    Call<Condition> getConditions(@Path("ApiKey") String apiKey, @Path("Country") String country, @Path("Locality") String locality);

    @GET("{ApiKey}/forecast10day/q/{Country}/{Locality}.json")
    Call<Forecast10Day> getForecast10Day(@Path("ApiKey") String apiKey, @Path("Country") String country, @Path("Locality") String locality);

}